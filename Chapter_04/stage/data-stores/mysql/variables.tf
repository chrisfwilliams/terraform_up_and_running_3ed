variable "db_username" {
  type        = string
  sensitive   = true 
  description = "The username for the database"
}

variable "db_password" {
  type        = string
  sensitive   = true 
  description = "The password for the database"
}

